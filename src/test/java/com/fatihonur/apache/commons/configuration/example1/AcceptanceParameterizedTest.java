package com.fatihonur.apache.commons.configuration.example1;

import static org.junit.Assert.*;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * @author qfatonu
 *
 */
@RunWith(Parameterized.class)
public class AcceptanceParameterizedTest {

	private XMLConfiguration config;
	private Integer startRnc;
	private Integer endRnc;
	private Integer share;
	private Integer order;
	private String irathom;
	private List<HierarchicalConfiguration> extGsmCellValues;

	public AcceptanceParameterizedTest(int order, int startRnc, int endRnc, int share, String irathom) {
		this.startRnc = startRnc;
		this.endRnc = endRnc;
		this.share = share;
		this.order = order;
		this.irathom = irathom;
	}
	

	@Parameters
	public static Collection<Object[]> data() {

		Object[][] data = new Object[][] { 
				{ 0, 1, 40, 300, "yes" }, 
				{ 1, 41, 41, 38, "yes" },
				{ 2, 42, 96, 1520, "" }, 
				{ 3, 97, 103, 3872, "" }, 
				{ 4, 104, 106, 5055, "" }, 
				{ 5, 107, 109, 9600, "" } 
			};
		return Arrays.asList(data);
	}

	@Before
	public void setUp() {
		try {
			this.config = new XMLConfiguration("config.xml");
			// accessing number of identical nodes via HierarchialConfiguration
			extGsmCellValues = config
					.configurationsAt("w1.relations.gsm.distribution.extGsmCell.value");
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testExtGsmCellAttributes() {
		int expectedStartRnc = extGsmCellValues.get(order).getInt("[@startRnc]");
		int expectedEndRnc = extGsmCellValues.get(order).getInt("[@endRnc]");
		int expectedShare = extGsmCellValues.get(order).getInt("[@share]");
			
		String expectedIrathom = extGsmCellValues.get(order).getString("[@irathom]");
		expectedIrathom = expectedIrathom == null ? "" : expectedIrathom; 

		System.out
				.format("ParameterizedTest: order=%d, actualStartRnc:%d, expectedStartRnc:%d %n",
						order, startRnc, expectedStartRnc);
		System.out
				.format("ParameterizedTest: order=%d, actualEndRnc:%d, expectedEndRnc:%d %n",
						order, endRnc, expectedEndRnc);

		System.out
		.format("ParameterizedTest: order=%d, actualShare:%d, expectedShare:%d %n",
				order, share, expectedShare);
		
		System.out
		.format("ParameterizedTest: order=%d, actualIrathom:%s, expectedItarhom:%s %n",
				order, irathom, expectedIrathom);
		
		System.out.println();

		assertEquals(startRnc, expectedStartRnc);
		assertEquals(endRnc, expectedEndRnc);
		assertEquals(share, expectedShare);
		assertEquals(irathom, expectedIrathom);

	}

}
