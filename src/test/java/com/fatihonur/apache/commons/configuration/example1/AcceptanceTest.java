package com.fatihonur.apache.commons.configuration.example1;

import static org.junit.Assert.*;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.junit.Before;
import org.junit.Test;

public class AcceptanceTest {
	
	XMLConfiguration config;
	
	@Before
	public void setup(){
		try {
			this.config = new XMLConfiguration("config.xml");
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetNetworkSize() {	
		String nSizeString = config.getString("w1.networkSize");		
		assertEquals("6996", nSizeString);
		
		int nSizeInt = config.getInt("w1.networkSize");		
		assertEquals(6996, nSizeInt);
	}
	
	@Test
	public void testRncInPoolFeature() {	
		String rncInPollFeature = config
				.getString("w1.rncInPoll[@feature]");
		assertEquals("on", rncInPollFeature);
	}

}
