package com.fatihonur.apache.commons.configuration.example1;

import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.ConfigurationNode;

/**
 * @author qfatonu
 * 
 */
public class App {
	public static void main(String[] args) {
		System.out.println("::ApacheCommonsConfiguration Usage Demo::");

		try {
			// First read your configuration file
			XMLConfiguration config = new XMLConfiguration("config.xml");

			// do something with config
			// here we access to a node value
			String networkSizeString = config.getString("w1.networkSize");
			int networkSizeInt = config.getInt("w1.networkSize");
			System.out.println("networkSizeString=" + networkSizeString);
			System.out.println("networkSizeInt=" + networkSizeInt);

			// accessing to node attribute
			String rncInPollFeature = config
					.getString("w1.rncInPoll[@feature]");
			System.out.println("rncInPollFeature=" + rncInPollFeature);

			// accessing to list of nodes
			List<String> members = config.getList("w1.rncInPoll.members.member");
			for (String s1 : members) {
				System.out.println("--rncInPoolMember=" + s1);
			}

			// accessing node in two different way
			HierarchicalConfiguration breakdown = config
					.configurationAt("w1.relations.utran.breakdown");
			System.out.println("breakdown.intra=" + breakdown.getInt("intra"));
			System.out.println("breakdown.inter="
					+ config.getString("w1.relations.utran.breakdown.inter"));

			// accessing number of identical nodes via HierarchialConfiguration
			List<HierarchicalConfiguration> extGsmCellValues = config
					.configurationsAt("w1.relations.gsm.distribution.extGsmCell.value");
			System.out.println("extGsmCellValues.size="
					+ extGsmCellValues.size());
			for (HierarchicalConfiguration sub : extGsmCellValues) {
				int startRnc = sub.getInt("[@startRnc]");
				int endRnc = sub.getInt("[@endRnc]");
				int share = sub.getInt("[@share]");
				if (sub.getString("[@irathom]") != null) {
					System.out.println("--irathom="
							+ sub.getBoolean("[@irathom]"));
				}
				System.out.format("++startRnc=%d, endRnc=%d, share=%d %n",
						startRnc, endRnc, share);
			}

			// accessing multiple nested nodes and multiple attributes as a list
			List<HierarchicalConfiguration> rncList = config
					.configurationsAt("w1.rncs.rnc");
			System.out.println("rncList.size=" + rncList.size());
			for (HierarchicalConfiguration sub : rncList) {
				int startRnc = sub.getInt("[@startRnc]");
				int endRnc = sub.getInt("[@endRnc]");
				int numOfRbs = sub.getInt("[@numOfRBS]");
				System.out.format("--startRnc=%d, endRnc=%d, numOfRbs=%d %n",
						startRnc, endRnc, numOfRbs);

				// ConfigurationNode node = sub.getRootNode().getChild(0);
				// //way1
				ConfigurationNode node = sub.configurationAt("rbs")
						.getRootNode(); // way2
				List<ConfigurationNode> y = node.getAttributes();
				for (ConfigurationNode cn : y) {
					System.out.format("name=%s, value=%s ", cn.getName(),
							cn.getValue());
				}
				System.out.println("");
			}


		} catch (ConfigurationException cex) {
			// something went wrong, e.g. the file was not found
			System.out.println(cex.getMessage());
		}

	}
}
